'''
Downloads CAViewer if not already downloaded.
'''

import os.path
import subprocess
import zipfile

DOWNLOAD_LINK = "https://gist.github.com/jedlimlx/a4ad9e4bddf1bc0fcfe220c4c150ff11/raw/02a656bc9dda3d6816d0bbcacb0c695b4032dc2c/CAViewer-Linux.zip"


def download():
    if (not os.path.exists(os.path.dirname(os.path.abspath(__file__)) + "/bin/CAViewer")):  # Checking if CAViewer exists
        print("Downloading CAViewer...")
        p = subprocess.Popen("wget " + DOWNLOAD_LINK,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        out = p.communicate()

        print("Unzipping...")
        with zipfile.ZipFile('CAViewer-Linux.zip', "r") as z:
            z.extractall(os.path.dirname(os.path.abspath(__file__)))

        print("Download complete!")
